#include <vector>
#include <memory>
#include <iostream>
using namespace std;

class C {
};

int main() {
    shared_ptr<C> sp;
    vector<weak_ptr<C>> wps;
    wps.push_back(sp);
    cout << "hello" << endl;
}
