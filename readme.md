## Thread on Twitter

https://twitter.com/tubo28/status/918041208265498624

## Versions

```
CLion 2017.2.3
Build #CL-172.4343.16, built on October 2, 2017
Licensed to (My Name)
Subscription is active until October 4, 2018
For educational use only.
JRE: 1.8.0_152-release-915-b12 amd64
JVM: OpenJDK 64-Bit Server VM by JetBrains s.r.o
Linux 4.4.0-97-generic
```

```
$ cat /etc/lsb-release
DISTRIB_ID=Ubuntu
DISTRIB_RELEASE=16.04
DISTRIB_CODENAME=xenial
DISTRIB_DESCRIPTION="Ubuntu 16.04.3 LTS"
```

```
$ g++ --version
g++ (Ubuntu 5.4.0-6ubuntu1~16.04.5) 5.4.0 20160609
Copyright (C) 2015 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
```

![img](Screenshot_2017-10-13_21-34-22.png)
